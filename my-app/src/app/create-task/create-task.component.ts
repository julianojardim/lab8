import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service';
import { FormBuilder, Validators } from '@angular/forms';
import { Todo } from '../todo';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit {
  users: any = [];
  taskForm;
  todo: Todo[];

  constructor(private apiService: ApiService, private fb: FormBuilder) {
    this.taskForm = this.fb.group({
      title: ['', Validators.required],
      completed: [''],
      userId: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.getUsers();
  }

  onSubmit(taskData) {
    this.apiService.postTodo(taskData).subscribe(
      data =>{
        this.todo = data;
        console.log(data);
    });
    //console.log(taskData);
  }

  getUsers() {
    this.apiService.getUsers().subscribe(data => this.users = data);
  }


}
