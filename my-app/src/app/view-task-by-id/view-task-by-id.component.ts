import { Component, OnInit, Input } from '@angular/core';
import { ApiService} from '../service';
import { FormBuilder, FormControl, ControlValueAccessor, Validators } from '@angular/forms'
import { ActivatedRoute } from '@angular/router';
import { Todo} from '../todo';
import { User} from '../user';

@Component({
  selector: 'app-view-task-by-id',
  templateUrl: './view-task-by-id.component.html',
  styleUrls: ['./view-task-by-id.component.css']
})

export class ViewTaskByIdComponent implements OnInit {
  todoData: any;
  users: User[];
  todos: Todo[];
  editTask;
  
  constructor(private apiService: ApiService, private route: ActivatedRoute, private fb: FormBuilder) {
    this.editTask = this.fb.group({
      title: ['', Validators.required],
      completed: [''],
      userId: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.getTodoId();
    this.getUserTodo();
  }

  getTodoId(){
    this.apiService.getTodoPorId(this.route.snapshot.params['id']).subscribe(data => {
      this.todoData = data;
    });
  }

  getUserTodo() {
    this.apiService.getUsers().subscribe(data => {
      this.users = data;
    });
  }

  onSubmit(taskData) {
    this.apiService.patchTodo(taskData, this.route.snapshot.params['id'])
      .subscribe(data => {
        this.todos = data;
        console.log(data);
      });
    //console.log(taskData);
  }

  }
