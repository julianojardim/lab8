import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewTarefasComponent } from './view-tarefas/view-tarefas.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';
import { ViewTaskByIdComponent } from './view-task-by-id/view-task-by-id.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { ViewTodoByUserComponent } from './view-todo-by-user/view-todo-by-user.component';
import {MatFormFieldModule} from '@angular/material/form-field';

@NgModule({
  declarations: [
    AppComponent,
    ViewTarefasComponent,
    ViewTaskByIdComponent,
    CreateTaskComponent,
    ViewTodoByUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatToolbarModule, 
    MatSelectModule
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent],
  exports: [MatButtonModule]
})
export class AppModule { }
