import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewTarefasComponent } from './view-tarefas/view-tarefas.component';
import { ViewTaskByIdComponent } from './view-task-by-id/view-task-by-id.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { ViewTodoByUserComponent } from './view-todo-by-user/view-todo-by-user.component';

const routes: Routes = [
  { path: '', component: ViewTarefasComponent },
  { path: 'todos/create', component: CreateTaskComponent },
  { path: 'todos/edit/:id', component: ViewTaskByIdComponent },
  { path: 'todos/user', component: ViewTodoByUserComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
