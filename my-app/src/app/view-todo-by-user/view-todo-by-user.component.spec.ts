import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTodoByUserComponent } from './view-todo-by-user.component';

describe('ViewTodoByUserComponent', () => {
  let component: ViewTodoByUserComponent;
  let fixture: ComponentFixture<ViewTodoByUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTodoByUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTodoByUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
