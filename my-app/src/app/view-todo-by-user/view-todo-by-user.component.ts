import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service';
import { FormBuilder, FormControl } from '@angular/forms';
import {Todo} from '../todo';

@Component({
  selector: 'app-view-todo-by-user',
  templateUrl: './view-todo-by-user.component.html',
  styleUrls: ['./view-todo-by-user.component.css']
})
export class ViewTodoByUserComponent implements OnInit {
  users: any = [];
  todos2: Todo[];
  todosUser: Todo[];
  displayedColumns = ['userId', 'title', 'completed'];
  tarefaIdForm;
  constructor(private apiService: ApiService, private formBuilder: FormBuilder) {
    this.tarefaIdForm = this.formBuilder.group({
      name: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.getUser();
    this.getTodos();

  }
  getTodos() {
    this.apiService.getTodos().subscribe(data => this.todos2 = data);
  }
  getUser() {
    this.apiService.getUsers().subscribe(data => this.users = data);

  }
  onSubmit(e) {
    console.log(e);
    const user = this.users.filter( a => { return a.name == e.name });
    console.log(user);
    this.todosUser = this.todos2.filter((userid) => { return userid.userId == user[0].id });
  }

}
