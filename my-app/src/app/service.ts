import {Injectable} from '@angular/core';
import {HttpClient,HttpHeaders,HttpErrorResponse} from '@angular/common/http';
import {Observable,throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';
import {User} from './user';
import {Todo} from './todo';


@Injectable({
    providedIn: 'root'
})
export class ApiService{
    urlRoot ='https://jsonplaceholder.typicode.com';
    options = {headers: new HttpHeaders().set('Content-type','application/json')};
    
    constructor(private http: HttpClient){}

    //TODOS
    getTodos():Observable<Todo[]>{
        return this.http.get<Todo[]>(this.urlRoot + '/todos').pipe(
            catchError(this.handleError)
          );
    }

    getTodoPorId(id:number):Observable<Todo>{
        return this.http.get<Todo>(this.urlRoot + '/todos' + `/${id}`).pipe(
            catchError(this.handleError)
          );
    }

    postTodo(todo:Todo):Observable<Todo[]>{
        return this.http.post<Todo[]>(this.urlRoot+ '/todos',todo,this.options).pipe(
            catchError(this.handleError)
          );
    }

    putTodo(todo:Todo,id:number):Observable<Todo[]>{
       return this.http.put<Todo[]>(this.urlRoot + '/todos'+`/${id}`,todo,this.options).pipe(
        catchError(this.handleError)
      ); 
    }

    patchTodo(todo:Todo, id:number):Observable<Todo[]>{
        return this.http.patch<Todo[]>(this.urlRoot + '/todos' + `/${id}`,todo,this.options).pipe(
            catchError(this.handleError)
          );
    }

    deleteTodo(id:number):Observable<{}>{
        return this.http.delete<Todo[]>(this.urlRoot + '/todos' + `/${id}`).pipe(
            catchError(this.handleError)
          );
    }

    //USERS
    getUsers():Observable<User[]>{
        return this.http.get<User[]>(this.urlRoot + '/users').pipe(
            catchError(this.handleError)
          );
    }

    getUserPorId(id:number):Observable<User>{
        return this.http.get<User>(this.urlRoot + '/users' + `/${id}`).pipe(
            catchError(this.handleError)
          );
    }
    
    postUser(user:User):Observable<User[]>{
        return this.http.post<User[]>(this.urlRoot+ '/users',user,this.options).pipe(
            catchError(this.handleError)
          );
    }
    putUser(user:User,id:number):Observable<User[]>{
       return this.http.put<User[]>(this.urlRoot + '/users'+`/${id}`,user,this.options).pipe(
        catchError(this.handleError)
      );
    }
    patchUser(user:User, id:number):Observable<User[]>{
        return this.http.patch<User[]>(this.urlRoot + '/users' + `/${id}`,user,this.options).pipe(
            catchError(this.handleError)
          );
    }
    deleteUser(id:number):Observable<{}>{
        return this.http.delete<User[]>(this.urlRoot + '/users' + `/${id}`).pipe(
            catchError(this.handleError)
          );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong.
          console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // Return an observable with a user-facing error message.
        return throwError(
          'Something bad happened; please try again later.');
      }
}