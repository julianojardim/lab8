import { Component, OnInit } from '@angular/core';
import {ApiService} from '../service';
import {Todo} from '../todo';


@Component({
  selector: 'app-view-tarefas',
  templateUrl: './view-tarefas.component.html',
  styleUrls: ['./view-tarefas.component.css']
})
export class ViewTarefasComponent implements OnInit {
  todos: Todo[];
  displayedColumns = ['userId', 'title', 'completed', 'editTask'];
  constructor(private apiService:ApiService) { }

  ngOnInit(): void {
  }
  getTarefas(){
    this.apiService.getTodos().subscribe(data =>this.todos = data);
  }

}
