import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTarefasComponent } from './view-tarefas.component';

describe('ViewTarefasComponent', () => {
  let component: ViewTarefasComponent;
  let fixture: ComponentFixture<ViewTarefasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTarefasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTarefasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
